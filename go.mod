module gitee.com/udox/golang-common

go 1.20

require go.uber.org/zap v1.24.0

require (
	github.com/lxn/win v0.0.0-20210218163916-a377121e959e // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
)
