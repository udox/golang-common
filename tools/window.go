package tools

import (
	"github.com/lxn/win"
	"syscall"
	"unsafe"
)

var (
	dllShell32       = syscall.NewLazyDLL("shell32.dll")
	procShellExecute = dllShell32.NewProc("ShellExecuteExW")

	dllKernel32     = syscall.NewLazyDLL("kernel32.dll")
	procCreateMutex = dllKernel32.NewProc("CreateMutexW")

	dllUser32 = syscall.NewLazyDLL("user32.dll")
	//setWindowPos = dllUser32.NewProc("SetWindowPos")
	findWindowW = dllUser32.NewProc("FindWindowW")
)

// SetWindowTop
//
//	@Description: 设置窗口置顶
//	@param pbool 是否置顶
//	@param title  窗口标题
//	@return bool
func SetWindowTop(pbool bool, title string) bool {
	hwnd := win.FindWindow(win.StringToBSTR("Chrome_WidgetWin_1"), win.StringToBSTR(title))

	if pbool == true {
		return win.SetWindowPos(hwnd, win.HWND_TOPMOST, 0, 0, 0, 0, win.SWP_NOMOVE|win.SWP_NOSIZE|win.SWP_SHOWWINDOW)
	} else {
		return win.SetWindowPos(hwnd, win.HWND_NOTOPMOST, 0, 0, 0, 0, win.SWP_NOMOVE|win.SWP_NOSIZE|win.SWP_SHOWWINDOW)
	}

}

func CheckMutex(name string) error {
	namePtr, err := syscall.UTF16PtrFromString(name)
	if err != nil {
		return err
	}

	//ret, _, err := procCreateMutex.Call(0, 1, uintptr(unsafe.Pointer(namePtr)))
	//h := syscall.Handle(ret)
	_, _, err = procCreateMutex.Call(0, 1, uintptr(unsafe.Pointer(namePtr)))

	switch err {
	case syscall.Errno(0):
		return nil

	//case syscall.ERROR_ALREADY_EXISTS:
	//	s, e := syscall.WaitForSingleObject(h, syscall.INFINITE) //这句是等另一个进程退出...才会返回...
	//	if s != syscall.WAIT_OBJECT_0 {
	//		return fmt.Errorf("wait failed %v: %v", s, e)
	//	}
	//
	//	return nil

	default:
		return err
	}
}

// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-findwindoww
//
// FindWindowW
//
//	@Description: 查找窗口
//	@param lpClassName
//	@param lpWindowName
//	@return uintptr
func FindWindowW(lpClassName, lpWindowName string) uintptr {
	var arg1 uintptr = 0
	if lpClassName > "" {
		arg1 = uintptr(unsafe.Pointer(str(lpClassName)))
	}
	ret, _, _ := findWindowW.Call(
		arg1,
		uintptr(unsafe.Pointer(str(lpWindowName))),
	)
	return ret
}
func str(s string) *uint16 {
	p, _ := syscall.UTF16PtrFromString(s)
	return p
}

/*
https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setwindowpos
hwnd HWND，欲定位的窗口句柄
　　hWndInsertAfter HWND，置于hwnd前面的窗口句柄。这个参数必须是窗口的句柄或是下面的值之一：
　  HWND_BOTTOM 将窗口置于其它所有窗口的底部
　　HWND_NOTOPMOST 将窗口置于其它所有窗口的顶部，并位于任何最顶部窗口的后面。如果这个窗口非顶部窗口，这个标记对该窗口并不产生影响
　　HWND_TOP 将窗口置于它所有窗口的顶部
　　HWND_TOPMOST 将窗口置于其它所有窗口的顶部，并位于任何最顶部窗口的前面。即使这个窗口不是活动窗口，也维持最顶部状态
x：
　　int，指定窗口新的X坐标
Y：
　　int，指定窗口新的Y坐标
cx：
　　int，指定窗口新的宽度
cy：
　　int，指定窗口新的高度
wFlags：
　　UINT，指定窗口状态和位置的标记。这个参数使用下面值的组合： SWP_DRAWFRAME 围绕窗口画一个框
　　SWP_FRAMECHANGED 发送一条WM_NCCALCSIZE消息进入窗口，即使窗口的大小没有发生改变。如果不指定这个参数，消息WM_NCCALCSIZE只有在窗口大小发生改变时才发送
　　SWP_HIDEWINDOW 隐藏窗口
　　SWP_NOACTIVATE 不激活窗口
　　SWP_NOCOPYBITS 屏蔽客户区域
　　SWP_NOMOVE 保持当前位置（X和Y参数将被忽略）
　　SWP_NOOWNERZORDER 不改变所有窗口的位置和排列顺序
　　SWP_NOREDRAW 窗口不自动重画
　　SWP_NOREPOSITION 与SWP_NOOWNERZORDER标记相同
　　SWP_NOSENDCHANGING 防止这个窗口接受WM_WINDOWPOSCHANGING消息
　　SWP_NOSIZE 保持当前大小（cx和cy会被忽略）
　　SWP_NOZORDER 保持窗口在列表的当前位置（hWndInsertAfter将被忽略）
　　SWP_SHOWWINDOW 显示窗口
	可以 　　SWP_NOMOVE ||
*/
